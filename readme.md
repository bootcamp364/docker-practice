Steps

**step0**: create a basic node js html and server

**step1**: pull mongo and mongo-express
`docker pull mongo`
`docker pull mongo-express`

**step2**: create custom docker network for mongo and mongo-express images to communicate: `docker network create mongo-network`

**step3**: run mongo docker image container in the network
`docker run -p 27017:27017 -d -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=welcome1 --name mongo_container --net mongo-network mongo`

**step4:** run mongo-express docker image container in the network
`docker run -d -p 8081:8081 -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin -e ME_CONFIG_MONGODB_ADMINPASSWORD=welcome1 --net mongo-network --name mongo-express_container -e ME_CONFIG_MONGODB_SERVER=mongo_container mongo-express`

**step5**: connect node JS with the database
