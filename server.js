let express = require('express');
let MongoClient = require('mongodb').MongoClient;
let path = require('path');
let fs = require('fs');
let cors = require('cors');

let app = express();

// middlewares
app.use(cors());
app.use(express.json());

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

// use when starting application locally
let mongoUrlLocal = 'mongodb://admin:welcome1@localhost:27017';

// use when starting application as docker container
let mongoUrlDocker = 'mongodb://admin:welcome1@mongodb';

// pass these options to mongo client connect request to avoid DeprecationWarning for current Server Discovery and Monitoring engine
let mongoClientOptions = { useNewUrlParser: true, useUnifiedTopology: true };

// "user" in demo with docker. "my-db" in demo with docker-compose
let databaseName = 'user-db';

app.post('/create-profile', (req, res) => {
  let userObj = req.body;

  MongoClient.connect(mongoUrlLocal, mongoClientOptions, (err, client) => {
    if (err) throw err;

    let db = client.db(databaseName);
    userObj['userid'] = 1;

    let myquery = { userid: 1 };
    let newvalues = { $set: userObj };

    db.collection('user').updateOne(
      myquery,
      newvalues,
      { upsert: true },
      function (err, res) {
        if (err) throw err;
        client.close();
      }
    );
  });
  // Send response
  res.send(userObj);
});

app.get('/get-profile', (req, res) => {
  let response = {};
  MongoClient.connect(mongoUrlLocal, mongoClientOptions, (err, client) => {
    if (err) throw err;

    let db = client.db(databaseName);

    db.collection('users').findOne({ userid: 1 }, (err, result) => {
      if (err) throw err;
      response = result;
      client.close();

      // Send response
      res.send(response ? response : {});
    });
  });
});

app.listen(3000, () => {
  console.log('app listening on port 3000');
});
